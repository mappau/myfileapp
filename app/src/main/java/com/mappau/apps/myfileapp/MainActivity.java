package com.mappau.apps.myfileapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    String name;
    TextView textView;
    EditText editText;
    TextView log;
    Button delete;
    SharedPreferences sharedPreferences;
    static final String FILENAME = "logfile.log";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.editText);
        textView = (TextView) findViewById(R.id.textView);
        log = (TextView) findViewById(R.id.logs);

        delete = (Button) findViewById(R.id.buttonDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFile(FILENAME);
                log.setText("");
            }
        });


        //Write to file
        String string = "App started at "+ Calendar.getInstance().getTime().toString()+"\n";

        FileOutputStream fos = null;
        try {
            fos = openFileOutput(FILENAME, Context.MODE_APPEND);
            fos.write(string.getBytes());
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        String fileContent="";
        // Read File Content
        try {
            FileInputStream inStream = openFileInput(FILENAME);

            if (inStream != null)
            {
                InputStreamReader inputreader = new InputStreamReader(inStream);
                BufferedReader buffreader = new BufferedReader(inputreader);
                String line = "";

                while ((line = buffreader.readLine()) != null)
                    fileContent=line+"\n"+fileContent;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }



        log.setText(fileContent);


        // get sharedPreferences
        sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        // get Name or "Kein Name" if no Name exists
        name = sharedPreferences.getString("Name", "Kein Name");

        // Fill Name into Views
        editText.setText(name);
        textView.setText("Hello "+name);

        //Listen for Changes
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //store new Text into sharedPreferences
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Name", s.toString());
                editor.commit();

                textView.setText("Hello " + s);
            }
        });





    }
}
